package tp2simplebis;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("config-simplebis.xml");
        QuestionService questionService = (QuestionService) context.getBean("questionService");
        String maQuestion = questionService.getQuestion();
        System.out.println(maQuestion);
    }
}
