package tp2simplebis;


import java.util.ArrayList;
import java.util.List;

public class QuestionsJavaAvance implements Questions {

    List<String> listQuestions = new ArrayList<>();


    public void setListQuestions(List<String> listQuestions) {
        this.listQuestions = listQuestions;
    }

    @Override
    public List<String> getListQuestions() {
        return listQuestions;
    }

    public QuestionsJavaAvance(){
    }
}
