package tp2;

import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class QuestionService {
    private Questions listQuestion;

    public QuestionService(){
    }

    public String getQuestion(){
        int i = (int)(Math.random() * this.listQuestion.questions().size());
        return this.listQuestion.questions().get(i);
    }

    public void setListQuestion(Questions listQuestion){
        this.listQuestion = listQuestion;
    }
}
