package tp2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("config-simple.xml");
        QuestionService questionService = (QuestionService) context.getBean("questionService");
        String maQuestion = questionService.getQuestion();
        System.out.println(maQuestion);
    }
}
